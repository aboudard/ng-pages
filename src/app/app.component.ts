import { Component, OnInit } from '@angular/core';
import { Configuration } from './dto/configuration';
import { ConfigService } from './services/config.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  title = 'ng-pages';
  configData: Configuration;

  constructor(private configService: ConfigService) {}

  ngOnInit(): void {
    this.configData = this.configService.configData;
  }

}
