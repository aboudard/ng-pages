import { Injectable } from '@angular/core';
import { Configuration } from '../dto/configuration';
import { HttpClient } from '@angular/common/http';
import { tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ConfigService {

  configData: Configuration;

  constructor(private http: HttpClient) { }

  getConf(): Promise<Configuration> {
    return this.http.get<Configuration>('configuration.json')
      .pipe(
        tap(result => {
          this.configData = result;
        })
      ).toPromise();
  }


}
