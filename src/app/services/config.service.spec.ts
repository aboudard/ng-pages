import { TestBed } from '@angular/core/testing';
import { mockConfiguration } from '../mock';
import {
  HttpClientTestingModule,
  HttpTestingController
} from '@angular/common/http/testing';
import { ConfigService } from './config.service';
import Spy = jasmine.Spy;

describe('ConfigService', () => {

let service: ConfigService;
let httpMock: HttpTestingController;

beforeEach(() => {
 TestBed.configureTestingModule({
   imports: [HttpClientTestingModule],
   providers: [
     ConfigService
   ]
 });
 service = TestBed.inject(ConfigService);
 httpMock = TestBed.inject(HttpTestingController);
});

afterEach(() => {
 httpMock.verify();
});

it('should be created', () => {
 expect(service).toBeTruthy();
});

it('should call config', () => {
 const dummyConf = mockConfiguration;
 service.getConf().then(res => {
   expect(res.app_version).toEqual(mockConfiguration.app_version);
 });
 const request = httpMock.expectOne('configuration.json');
 expect(request.request.method).toBe('GET');
 request.flush(dummyConf);
 httpMock.verify();
});


});
